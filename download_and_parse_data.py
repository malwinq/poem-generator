from _cleaning_options.cleaner import super_cleaner
from gutenberg.acquire import load_etext
from gutenberg.cleanup import strip_headers

""" Download the data from Gutenberg, find poems, cleans the headers etc. and save new data to one txt file """
""" 666 books, 138214229 chars """

# params
file1 = 'dataset_v0.txt'
file2 = 'dataset_v1.txt'
file3 = 'GUTENBERG_ALL.txt'
n = 3

"""
# has to be done on first run
import nltk
nltk.download('punkt')
"""

blank = ' ' * n
id_list = []
prev_line = ''

# find indexes of books with poetry
with open(file3, 'r') as file:
    lines = file.readlines()

    for line in lines:
        if 'poe' in line or 'Poe' in line:
            ind = line.rfind(blank) + n
            if ind > n:
                try:
                    num = int(line[ind:])
                    id_list.append(num)
                except:
                    # check the previous line
                    ind = prev_line.rfind(blank) + n
                    if ind > n:
                        try:
                            num = int(prev_line[ind:])
                            id_list.append(num)
                        except:
                            pass
            prev_line = line

print('Found', len(id_list), 'books with poetry')

text_to_save = []
n_ch = 0
n_f = 0

file = open(file1, 'w')

# loop for loading and cleaning the text
for id_nr in id_list:

    try:
        text = load_etext(id_nr)
        text = strip_headers(text).strip()
        text = super_cleaner(text)

        n_ch += file.write(text)
        n_f += 1
    except:
        continue
    print('File', n_f, 'saved')

print('Saved', n_f, 'files from Gutenberg with', n_ch, 'chars to', file1, 'file')
file.close()
i = 0

# loop for deleting lines '[deleted]'
with open(file1, 'r') as input:
    with open(file2, 'w') as output:
        for line in input:
            if '[deleted]' not in line:
                output.write(line)
            else:
                i += 1

print('Deleted', i, 'lines')
print('Parsing done.')
