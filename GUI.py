from tkinter import *
from tkinter.scrolledtext import ScrolledText
import neural_network as network

""" GUI for generator """

nn = network.Network()


def start_nn_command():
    global nn
    nn.initialize()


def predict_love():
    global nn
    nn.predict_love()


def predict_nature():
    global nn
    nn.predict_nature()


if __name__ == '__main__':

    font = ('Arial Bold', 20)

    window = Tk()
    window.title('POEM GENERATOR')
    window.geometry('700x700')

    lbl0 = Label(window, text='Enter the phrase',
                 font=font, fg='blue')
    lbl0.pack(fill=X, pady=30, padx=30)

    text = ScrolledText(window, width=10, height=5)
    text.pack(fill=X, pady=10, padx=60)

    lbl1 = Label(window, text='Choose the poetry type',
                 font=font, fg='blue')
    lbl1.pack(fill=X, pady=30, padx=10)

    # buttons
    # TODO - radio buttons
    btn_love = Button(window, text='Love',
                      bg='blue4', fg='white',
                      height=2, width=10,
                      font=font, command=predict_love)
    btn_love.pack(side=LEFT, padx=70, pady=0)
    btn_nature = Button(window, text='Nature',
                        bg='blue4', fg='white',
                        height=2, width=10,
                        font=font, command=predict_nature)
    btn_nature.pack(side=RIGHT, padx=70, pady=0)

    # btn_generate = Button(window, text='GENERATE',
    #                       bg='blue4', fg='white',
    #                       height=2, width=10,
    #                       font=font, command=predict_nature)
    # btn_generate.pack(padx=10, pady=100)

    window.mainloop()
