# Poem generator

Generating poems based on polish and english poetry.

Requirements:
* TensorFlow 1.13.1
* Keras 2.2.4
* TkInter 8.6
* NumPy 1.16.2
* Berkeley DB-5.3.28 
* Gutenberg + gutenberg-cleaner (see dataset below)
* NLTK 3.4.5

Dataset:

From kaggle contest: 

https://www.kaggle.com/ultrajack/modern-renaissance-poetry

Gutenberg project poetry parsed with the library gutenberg-cleaner: 

https://github.com/kiasar/gutenberg_cleaner

If there is an error on installing Gutenberg:

```Can't find a local Berkeley DB installation.(suggestion: try the --berkeley-db=/path/to/bsddb option)```

Try:

``` sudo apt-get install libdb-dev ```

``` sudo pip install bsddb3 ```

This fixed the problem for me.

The model of NN was trained using Colab.

